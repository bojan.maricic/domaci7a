<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Registracija</title>	
	<meta name="description" content="Dobro došli na našu web stranicu. Ovde možete pogledati našu ponudu novih i polovnih automobila različitih proizvođača.">
	<meta name="keywords" content="#">
	<meta name="author" content="Bojan">
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<meta property="og:title" content="Car Dealer"/>
	<meta property="og:description" content="Novi i polovni automobili" />
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all"/>

	</head>	
	<body>
		<div class="wrapper">			
			<!--CONTAINER-->
			<div class="container">
				<div class="registracija">
					<div class="title">
						<h2>Registracija</h2>		
						<p>Registraciona prijava</p>	
					</div>	
					<div class="registracija-box">
						<h4>Vaši podaci iz registracione prijave su:</h4>  

<?php
// define variables and set to empty values
$nameErr = $lastnameErr = $emailErr = $genderErr = $websiteErr = $birthdayErr = "";
$name = $lastname = $email = $gender = $comment = $website = $birthday = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Unesi ime";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
	if (strlen($name) < 3){
	$nameErr = "Ime mora imati više od 2 karaktera";
	}

  }
  
   if (empty($_POST["lastname"])) {
    $lastnameErr = "Unesi prezime";
  } else {
    $lastname = test_input($_POST["lastname"]);
    // check if last name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$lastname)) {
      $lastnameErr = "Only letters and white space allowed"; 
    }
	if (strlen($lastname) < 3){
	$lastnameErr = "Prezime mora imati više od 2 karaktera";
	}
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Unesi e-mail";
  } else if (!check_email_address($_POST["email"])) {
    $emailErr = "Email nije ispravan";
    
  } else {
     $email = test_input($_POST["email"]);
  }
  
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Neispravan URL"; 
    }
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Unesi pol";
  } else {
    $gender = test_input($_POST["gender"]);
  }
  
  if (empty($_POST["birthday"])) {
    $birthdayErr = "Unesi datum rođenja";
  } else {
    $birthday = test_input($_POST["birthday"]);
  }
   
  if (!empty($nameErr) or !empty($lastnameErr) or !empty($emailErr) or !empty($websiteErr) or !empty($commentErr) or !empty($genderErr) or !empty($birthdayErr)) {
    $params = "name=" . urlencode($_POST["name"]);
	$params = "&lastname=" . urlencode($_POST["lastname"]);
    $params .= "&email=" . urlencode($_POST["email"]);
    $params .= "&website=" . urlencode($_POST["website"]);
    $params .= "&comment=" . urlencode($_POST["comment"]);
    $params .= "&gender=" . urlencode($_POST["gender"]);
	$params .= "&birthday=" . urlencode($_POST["birthday"]);

    $params .= "&nameErr=" . urlencode($nameErr);
	$params .= "&lastnameErr=" . urlencode($lastnameErr);
    $params .= "&emailErr=" . urlencode($emailErr);
    $params .= "&websiteErr=" . urlencode($websiteErr);
    $params .= "&commentErr=" . urlencode($commentErr);
    $params .= "&genderErr=" . urlencode($genderErr);
	$params .= "&birthdayErr=" . urlencode($birthdayErr);
    
    header("Location: index.php?" . $params);
  }  else {
	  
    echo "Ime: " . $_POST['name'];
    echo "<br>";
    
	echo "Prezime: " . $_POST['lastname'];
    echo "<br>";
	
    echo "Email: " . $_POST['email'];
    echo "<br>";
    
    echo "Website: " . $_POST['website'];
    echo "<br>";
    
    echo "Komentar: " . $_POST['comment'];
    echo "<br>";
    
    echo "Pol: " . $_POST['gender'];  
    echo "<br>";
    echo "<br>";
	
	echo "Datum rođenja: " . $_POST['birthday'];
    echo "<br>";
	echo "Vi ste rođeni u " . date("l", strtotime($birthday));
	echo "<br>";
    echo "<br>";
	
    echo "<a href=\"index.php\">Povratak na registracionu prijavu</a>";
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function check_email_address($email) {
    // pravimo niz:
    $email_array = explode("@", $email);

    // dobar email format
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    
      return false;
    }
    else if(!preg_match('/\@gmail.com/', $email)) {
	//  email nije gmail
      return false;
    }
    else if(strlen($email_array[0])<6) {
	// ime u emailu ima manje od 6 znakova
	return false;
    } 
    else {
        //svi uslovi ispunjeni
	return true;
    }
}

?>

					</div>
				</div>	
			</div>
	</body>
</html>
