<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Car Dealer | Registracija</title>	
	<meta name="description" content="Dobro došli na našu web stranicu. Ovde možete pogledati našu ponudu novih i polovnih automobila različitih proizvođača.">
	<meta name="keywords" content="#">
	<meta name="author" content="Bojan">
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<meta property="og:title" content="Car Dealer"/>
	<meta property="og:description" content="Novi i polovni automobili" />
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all"/>
	<link rel="stylesheet" href="css/font-awesome/font-awesome.min.css" media="screen"/>
	<link rel="stylesheet" href="css/flaticon/flaticon.css" media="screen"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/javascript.js"></script>

	</head>
	
	<body>
		<div class="wrapper">
		
			<!--HEADER-->
			<?php include ('header.html') ?>
			<!--KRAJ HEADER-->
			
			<!--CONTAINER-->
			<div class="container">
							
				<!--REGISTRACIJA-->
				<div class="registracija">
					<div class="title">
					<h2>Registracija</h2>		
					<p>Registraciona prijava</p>	
					</div>	

<?php
    $name = $lastname = $email = $website = $comment = $gender = "";
    $nameErr = $lastnameErr = $emailErr = $genderErr = $websiteErr = $birthdayErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
	if (isset($_GET['lastname'])) { $lastname = $_GET['lastname']; }
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['website'])) { $website = $_GET['website']; }
    if (isset($_GET['comment'])) { $comment = $_GET['comment']; }
    if (isset($_GET['gender'])) { $gender = $_GET['gender']; }
	if (isset($_GET['birthday'])) { $birthday = $_GET['birthday']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
	if (isset($_GET['lastnameErr'])) { $lastnameErr = $_GET['lastnameErr']; }
    if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
    if (isset($_GET['websiteErr'])) { $websiteErr = $_GET['websiteErr']; }
    if (isset($_GET['commentErr'])) { $commentErr = $_GET['commentErr']; }
    if (isset($_GET['genderErr'])) { $genderErr = $_GET['genderErr']; }
	if (isset($_GET['birthdayErr'])) { $birthdayErr = $_GET['birthdayErr']; }
    
?>

<form method="post" action="register.php">
  <div class="registracija-box">
	
	<h4>Sva polja označena sa <span>*</span> morate obavezno da popunite</h4>
	
	<p>Ime <span>*  <?php echo $nameErr;?></span></p>
	<input type="text" name="name" value="<?php echo $name;?>">

	<p>Prezime <span>*  <?php echo $lastnameErr;?></span></p>
	<input type="text" name="lastname" value="<?php echo $lastname;?>">
  
	<p>E-mail <span>*  <?php echo $emailErr;?></span></p>
	<input type="text" name="email" value="<?php echo $email;?>">
 
	<p>Website <span><?php echo $websiteErr;?></span></p>
	<input type="text" name="website" value="<?php echo $website;?>">
 
	<p>Komentar</p>
	<textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
  
	<p>Pol <span>*  <?php echo $genderErr;?></span></p>
  <p><input class="checkbox" type="checkbox" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="ženski">Ženski</p>
  <p><input class="checkbox" type="checkbox" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="muški">Muški</p>
  
<p>Datum rođenja <span>*  <?php echo $birthdayErr;?></span></p>
 <input type="date" name="birthday" value="<?php echo $birthday;?>" />
  
  <button type="submit" name="submit" value="Submit"> Pošalji</button>
</form>

</div>
				</div>	
				<!--KRAJ REGISTRACIJE-->
				
				
			</div>
			<!--KRAJ SIDEBAR-->
			
				<!--FOOTER-->
			<?php include ('footer.html') ?>
				<!--KRAJ FOOTER-->
		</div>
	</body>
</html>